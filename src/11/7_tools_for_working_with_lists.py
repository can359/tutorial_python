# https://docs.python.org/es/3/tutorial/stdlib2.html#tools-for-working-with-lists

from array import array
from collections import deque
import bisect
from heapq import heapify, heappop, heappush

a = array('H', [400, 10, 700, 22222])
print(sum(a))
print(a[1:3])

print()

d = deque(['task1', 'task2', 'task3'])
d.append('task4')
print('Handling', d.popleft())

# unsearched = deque([starting_node])
# def breadth_first_search(unsearched):
#     node = unsearched.popleft()
#     for m in gen_moves(node):
#         if is_goal(m):
#             return m
#         unsearched.append(m)

print()

scores = [(100, 'perl'), (200, 'tcl'), (400, 'lua'), (500, 'python')]
bisect.insort(scores, (300, 'ruby'))
print(scores)

print()

data = [1, 3, 5, 7, 9, 2, 4, 6, 8, 0]
heapify(data)                                   # rearrange the list into heap order
heappush(data, -5)                              # add a new entry
print([heappop(data) for i in range(3)])        # fetch the three smallest entries
