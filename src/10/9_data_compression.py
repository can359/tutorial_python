# https://docs.python.org/es/3/tutorial/stdlib.html#data-compression

import zlib

s = b'witch which has which witches wrist watch'
print(len(s))

t = zlib.compress(s)
print(len(t))

print(zlib.decompress(t))

print(zlib.crc32(s))
