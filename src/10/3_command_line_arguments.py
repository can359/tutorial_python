# https://docs.python.org/es/3/tutorial/stdlib.html#command-line-arguments

import sys
import argparse

print(sys.argv)

print()

parser = argparse.ArgumentParser(prog = 'top',
    description = 'Show top lines from each file')

parser.add_argument('filesnames', nargs='+')
parser.add_argument('-l', '--lines', type=int, default=10)
args = parser.parse_args()
print(args)
