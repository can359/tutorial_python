# https://docs.python.org/es/3/tutorial/stdlib.html#operating-system-interface

import os
import shutil

print(os.getcwd())  # Return the current working directory
print(os.chdir('src'))  # Change current workinf directory
print(os.system('mkdir today'))  # Run the command mkdir in the system shell

print()

print(dir(os))
print(help(os))

print()

print(shutil.copyfile('myfile.txt', 'myfile2.txt'))
print(shutil.move('/build.executables', 'installdir'))
