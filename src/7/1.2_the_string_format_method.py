# https://docs.python.org/es/3/tutorial/inputoutput.html#the-string-format-method

print('We are the {} who say "{}!"'.format('knights', 'Ni'))

print()

print('{0} and {1}'.format('spam', 'eggs'))
print('{1} and {0}'.format('spam', 'eggs'))

print()

print('This {food} is {adjective}.'.format(
    food='spam', adjective='absolutely horrible'
))

print()

print('The story of {0}, {1}, and {other}.'.format(
    'Bill', 'Manfred', other='Georg'
))

print()

table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 8637678}
print('Jack: {0[Jack]:d}; Sjoerd: {0[Sjoerd]:d}; Dcab: {0[Dcab]:d}'.format(table))
print('Jack: {Jack:d}; Sjoerd: {Sjoerd:d}; Dcab: {Dcab:d}'.format(**table))

print()

for x in range(1, 11):
    print('{0:2d} {1:3d} {2:4d}'.format(x, x*x, x*x*x))
