# https://docs.python.org/3/tutorial/datastructures.html#using-lists-as-stacks

stack = [3, 4, 5]
stack.append(6)
stack.append(7)
print(stack)

print(stack.pop())

print(stack)

print(stack.pop())
print(stack.pop())

print(stack)
