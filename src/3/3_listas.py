# https://docs.python.org/3/tutorial/introduction.html#lists

squares = [1, 4, 9, 16, 25]

print("1)", squares)

print("2)", squares[0]) # indexing returns the item

print("3)", squares[-1])

print("4)", squares[-3:]) # slicing returns a new list

print("5)", squares + [36, 49, 64, 81, 100])

cubes = [1, 8, 27, 65, 125] # something's wrong here
print("6)", 4 ** 3) # the cube of 4 is 64, not 65!
cubes[3] = 64 # replace the wrong value
print("7)", cubes)

cubes.append(216) # add the cube of 6
cubes.append(7 ** 3) # and the cube of 7
print("8)", cubes)

letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
print("9)", letters)

# replace some values
letters[2:5] = ['C', 'D', 'E']
print("10)", letters)

# now remove them
letters[2:5] = []
print("11)", letters)

# clear the list by replacing all the elements with an empty list
letters[:] = []
print("12)", letters)

letters = ['a', 'b', 'c', 'd']
print("13)", len(letters))

a = ['a', 'b', 'c']
n = [1, 2, 3]
x = [a, n]
print("14)", x)
print("15)", x[0])
print("16)", x[0][1])
