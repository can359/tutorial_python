# https://docs.python.org/3/tutorial/introduction.html#numbers

print("1)", 2 + 2)

print("2)", 50 - 5 * 6)

print("3)", (50 - 5 * 6) / 4)

print("4)", 8 / 5) # division always returns a floating point number

print("5)", 17 / 3) # classic division returns a float

print("6)", 17 // 3) # floor division discards the fractional part

print("7)", 17 % 3) # the % operator returns the remainder of the division

print("8)", 5 * 3 + 2) # floored quotient * divisor + remainder

print("9)", 5 ** 2) # 5 squared

print("10)", 2 ** 7) #2 to the power of 7

width = 20
height = 5 * 9
print("11)", width * height)

print("12)", 4 * 3.75 - 1)
