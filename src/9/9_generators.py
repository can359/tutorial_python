# https://docs.python.org/es/3/tutorial/classes.html#generators

def reverse(data):
    for index in range(len(data) - 1, -1, -1):
        yield data[index]

for char in reverse('golf'):
    print(char)
