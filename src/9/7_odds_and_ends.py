# https://docs.python.org/es/3/tutorial/classes.html#odds-and-ends

class Employee:
    pass

john = Employee()   # Create an empty employee record

# Fill the fields of the recod
john.name = "John Doe"
john.dept = "computer lab"
john.salary = 1000
