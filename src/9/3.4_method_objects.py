# https://docs.python.org/es/3/tutorial/classes.html#method-objects

class MyClass:
    """A simple example class"""
    i = 12345

    def f(self):
        return 'hello world'

x = MyClass()
print(x.f())

print()

xf = x.f
while True:
    print(xf())
