# https://docs.python.org/es/3/tutorial/classes.html#inheritance

class DerivedClassName(BaseClassName):
    '''
    <statement - 1>
    .
    .
    .
    <statement - N>
    '''

class DerivedClassName(modname.BaseClassName):
    pass
