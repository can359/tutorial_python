# https://docs.python.org/3/tutorial/controlflow.html#arbitrary-argument-lists

def write_multiple_items(file, separator, *args):
    file.write(separator.join(args))

def concat(*args, sep="/"):
    return sep.join(args)

print(concat("earth", "mars", "venus"))
print()
print(concat("eartg", "mars", "venus", sep="."))
