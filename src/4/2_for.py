# https://docs.python.org/3/tutorial/controlflow.html#for-statements

# Measure some strings:
words = ['cat', 'window', 'defenestrate']

for w in words:
    print(w, len(w))
