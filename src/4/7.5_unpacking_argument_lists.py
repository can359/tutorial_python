# https://docs.python.org/3/tutorial/controlflow.html#unpacking-argument-lists

print(list(range(3, 6))) # normal call with separate arguments

print()

args = [3, 6]
print(list(range(*args))) # call arguments unpacked from a list

print()

def parrot(voltage, state='a stiff', action='voom'):
    print("-- This parrot wouldn't", action, end=' ')
    print("if you put", voltage, "volts through it.", end=' ')
    print("E's", state, "!")

d = {"voltage": "four million", "state": "bleedin' demised", "action": "VOOM"}
parrot(**d)
