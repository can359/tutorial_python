# https://docs.python.org/3/tutorial/controlflow.html#the-range-function

for i in range(5):
    print(i)

print('')

for i in range(5, 10):
    print(i)

print('')

for i in range(0, 10, 3):
    print(i)

print('')

for i in range(-10, -100, -30):
    print(i)

print('')

a = ['Mary', 'had', 'a', 'little', 'lamb']
for i in range(len(a)):
    print(i, a[i])

print('')

print(range(10))

print('')

print(sum(range(4))) # 0 + 1 + 2 + 3

print('')

print(list(range(4)))
