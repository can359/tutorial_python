# https://docs.python.org/3/tutorial/controlflow.html#documentation-strings

def my_function():
    """Do nothing, but document it.

    No, really, it doesn't do anything.
    """
    pass

print(my_function.__doc__)
