# https://docs.python.org/3/tutorial/controlflow.html#pass-statements

while True:
    pass # Busy-wait for keyboard interrupt (Ctrl+C)

class MyEmptyClass:
    pass

def initlog(*args):
    pass # Remember to implement this!
